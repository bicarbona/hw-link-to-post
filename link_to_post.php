<?php
/**
 * Plugin Name: HW Link To Post
 * Plugin URI:  https://bitbucket.org/bicarbona/hw-link-to-post
 * Description: Headway - Link to Post
 * Version:     0.3.2
 * Author:      bicarbona
 * Author URI:  
 * License:     GPLv2+
 * Text Domain: link_to_post
 * Domain Path: /languages
 * Bitbucket Plugin URI: https://bitbucket.org/bicarbona/hw-link-to-post
* Bitbucket Branch: master
*/


define('LINK_TO_POST_BLOCK_VERSION', '0.1.0');

/**
 * Everything is ran at the after_setup_theme action to insure that all of Headway's classes and functions are loaded.
 **/
add_action('after_setup_theme', 'link_to_post_register');
function link_to_post_register() {

	if ( !class_exists('Headway') )
		return;

	require_once 'includes/Block.php';
	require_once 'includes/BlockOptions.php';
	//require_once 'design-editor-settings.php';

	return headway_register_block('link_to_post_block', plugins_url(false, __FILE__));
}

/**
 * Prevent 404ing from breaking Infinite Scrolling
 **/
add_action('status_header', 'link_to_post_prevent_404');
function link_to_post_prevent_404($status) {

	if ( strpos($status, '404') && get_query_var('paged') && headway_get('pb') )
		return 'HTTP/1.1 200 OK';

	return $status;
}

/**
 * Prevent WordPress redirect from messing up featured board pagination
 */
add_filter('redirect_canonical', 'link_to_post_redirect');
function link_to_post_redirect($redirect_url) {

	if ( headway_get('pb') )
		return false;

	return $redirect_url;
}