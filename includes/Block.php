<?php

class link_to_post_block extends HeadwayBlockAPI {

    public $id = 'link_to_post';
    public $name = 'Link To Post';
    protected $show_content_in_grid = true;

    public $options_class = 'link_to_postBlockOptions';
    public $description = 'Link to post';
    
	// function enqueue_action($block_id) {

		/* CSS */
	//	wp_enqueue_style('headway-pin-board', plugins_url(basename(dirname(__FILE__))) . '/css/pin-board.css');		

		/* JS */
	//	wp_enqueue_script('headway-pin-board', plugins_url(basename(dirname(__FILE__))) . '/js/pin-board.js', array('jquery'));		

	// }
	
	// public static function init_action($block_id, $block) 
    // {

    // }


    public static function enqueue_action($block_id, $block, $original_block = null)
    {

    }


    // public static function dynamic_css($block_id, $block, $original_block = null)
    // {

    // }


	// function dynamic_js($block_id, $block = false) {
	// 
	// 	if ( !$block )
	// 		$block = HeadwayBlocksData::get_block($block_id);
	// 
	// 	$js = "
	// 	jQuery(document).ready(function() {
	// 		
	// 	});
	// 	";
	// 
	// 	return $js;
	// 
	// }

    public function setup_elements() {
        $this->register_block_element(array(
            'id' => 'paragraph',
            'name' => 'P',
            'selector' => '.block-type-link_to_post p',
           // 'properties' => array('property1', 'property2', 'property3'),
            'states' => array(
                'Hover' => '.block-type-link_to_post p:hover',
               // 'Hover' => '.my-selector1:hover',
        //        'Clicked' => '.my-selector1:active'
                )
            ));

        $this->register_block_element(array(
				'id' => 'text-strong',
				'parent' => 'paragraph',
				'name' => 'strong',
				'selector' => '.block-type-link_to_post strong',
				//'inherit-location' => 'block-content-sub-heading'
		));

		$this->register_block_element(array(
				'id' => 'text-italic',
				'parent' => 'paragraph',
				'name' => 'italic',
				'selector' => '.block-type-link_to_post i, .block-type-link_to_post em',
				//'inherit-location' => 'block-content-sub-heading'
		));

			$this->register_block_element(array(
				'id' => 'link',
				//'parent' => 'heading',
				'name' => 'Link',
				'selector' => '.block-type-link_to_post a',
				//'inherit-location' => 'block-content-heading'
				'states' => array(
				'Hover' => '.block-type-link_to_post a:hover', 
				'Clicked' => '.block-type-link_to_post a:active'
				)
			));

		$this->register_block_element(array(
			'id' => 'heading',
			'name' => 'Heading',
			'selector' => '.block-type-link_to_post h3, .block-type-link_to_post h2, .block-type-link_to_post h1',
			'inherit-location' => 'default-heading'
		));

			$this->register_block_element(array(
				'id' => 'heading-h1',
				'parent' => 'heading',
				'name' => 'H1',
				'selector' => '.block-type-link_to_post h1',
				'inherit-location' => 'block-content-heading',
				'parent' => 'heading'
			));

			$this->register_block_element(array(
				'id' => 'heading-h2',
				'parent' => 'heading',
				'name' => 'H2',
				'selector' => '.block-type-link_to_post h2',
				'inherit-location' => 'block-content-heading'
			));

			$this->register_block_element(array(
				'id' => 'heading-h3',
				'parent' => 'heading',
				'name' => 'H3',
				'selector' => '.block-type-link_to_post h3',
				'inherit-location' => 'block-content-heading'
			));

		/**
		Subheading
		**/
		$this->register_block_element(array(
			'id' => 'sub-heading',
			'name' => 'Sub Heading',
			'selector' => '.block-type-link_to_post h4, .block-type-link_to_post h5, .block-type-link_to_post h6',
			'inherit-location' => 'default-sub-heading'
		));

			$this->register_block_element(array(
				'id' => 'sub-heading-h4',
				'parent' => 'sub-heading',
				'name' => 'H4',
				'selector' => '.block-type-link_to_post h4',
				'inherit-location' => 'block-content-sub-heading'
			));

			$this->register_block_element(array(
				'id' => 'sub-heading-h5',
				'parent' => 'sub-heading',
				'name' => 'H5',
				'selector' => '.block-type-link_to_post h5',
				'inherit-location' => 'block-content-sub-heading'
			));

			$this->register_block_element(array(
				'id' => 'sub-heading-h6',
				'parent' => 'sub-heading',
				'name' => 'H6',
				'selector' => '.block-type-link_to_post h6',
				'inherit-location' => 'block-content-sub-heading'
			));
		 /**
		Images
		 **/

		$this->register_block_element(array(
			'id' => 'entry-content-images',
			'name' => 'Body Images',
			'selector' => '.block-type-link_to_post img',
			'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow')
		));

		/**
		CTA
		**/

		$this->register_block_element(array(
			'id' => 'cta-link',
			'name' => 'CTA',
			'selector' => '.cta',
			'states' => array(
				'Hover' => '.cta:hover', 
				'Clicked' => '.cta:active'
			)
			//'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow')
		));
		
		$this->register_block_element(array(
			'id' => 'entry-content-font-icon',
			'name' => 'Font Icon',
			'selector' => '.block-type-link_to_post i.fa',
			//'properties' => array('background', 'borders', 'padding', 'corners', 'box-shadow')
		));

    }

    function head($block)	{
// ziskam data akualneho bloku
		$block  = HeadwayBlocksData::get_block($args['block_id']);

			// ziskam data konkretneho OPTIONS 
			//$include_file = HeadwayBlockAPI::get_setting($block, 'for-mobilex', array() );

		return HeadwayBlockAPI::get_setting($block, 'title-html-tag', 'h1');    	# code...
    }

/**

TODO
	funkcia nezobrazuje parameter z options....
	skusal som vsetko mozne takze jedine ze prerobit... podla (content / article builder... )
	ale pozor lebo tam je to troska inac /  je tam vytvorena trieda... cize sa neda pouzit viacnasobne ta ista....
	
**/
	function article_title($block) {
		$html_tag = parent::get_setting($block, 'title-html-tag', 'h2');
		$linked = parent::get_setting($block,'title-link', true);
		$shorten = parent::get_setting($block,'title-shorten', true);

		/* Shorten Title */
		$title_text = get_the_title($id);
		$title_length = mb_strlen($title_text);
		$limit = parent::get_setting($block,'title-limit', 20);
		$title = substr($title_text, 0, $limit);
		if ($title_length > $limit) 
			$title .= "...";

		if (!$shorten)
			$title = get_the_title($id);

		if($linked)
			return '<' . $html_tag . ' class="entry-title">
			<a href="'. get_post_permalink($id) .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title .'</a>
		</' . $html_tag . '>';
		
		return '<' . $html_tag . ' class="entry-title">
			'. $title .'
		</' . $html_tag . '>';

		return $html_tag;
	}

    public function content($block) {

		$html_tag = parent::get_setting($block, 'title-html-tag', 'h2');

 	wp_reset_query();
 
	$query_args = array();
 
 	// WP_Query arguments

	$query_args['post__in'] = parent::get_setting($block, 'post-id', array());
	$query_args['post_type'] = parent::get_setting($block, 'post-type', false);

	//echo 	$post_type = HeadwayBlockAPI::get_setting($block, 'post-type');

	//$query_args['post_type'] = 'content_block';

 // The Query
 $query = new WP_Query( $query_args );
// $query = new WP_Query( array( 'post_type' => 'post', 'postin' => array( 1 ) ) );

	while ( $query->have_posts() ) {$query->the_post();

/**
title
**/
		$linked = parent::get_setting($block,'title-link', false);
		$shorten = parent::get_setting($block,'title-shorten', false);
		$hide_edit_post_link = parent::get_setting($block,'hide-edit-post-link', false);
		$hide_title = parent::get_setting($block,'hide-title', false);

		/* Shorten Title */
		$title_text = get_the_title();
		$title_length = mb_strlen($title_text);
		$limit = parent::get_setting($block,'title-limit', 20);
		$title = substr($title_text, 0, $limit);

	if(!$hide_title){
		if ($title_length > $limit) 
			$title .= "...";

		if (!$shorten)
			$title = get_the_title();

		if(!$linked)
			$main_title = '<' . $html_tag . ' class="entry-title">'. $title.'</' . $html_tag . '>';

		if($linked)
			$main_title = '<' . $html_tag . ' class="entry-title">
			<a href="'. get_post_permalink() .'" rel="bookmark" title="'. the_title_attribute (array('echo' => 0) ) .'">'. $title .'</a>
		</' . $html_tag . '>';

		echo $main_title;

	}
	if(the_content()){

		the_content();
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  			the_post_thumbnail();
		}
	}

/**
CTA

TODO opravit tak aby 
the_title() bolo title clanku na ktory odkazuje a nie content_BLOCKu

detto edit link.... ten zas nech odkazuje na content_ block!!!
**/

if (function_exists('get_field')){
if( get_field('cta_content_text') ) {
    echo get_field('cta_content_text');
}
	$posts = get_field('cta_link_url');
	if ( $posts ): ?>
	    <?php foreach ( $posts as $post ) : setup_postdata( $post ); ?>
	        <a href="<?php the_permalink(); ?>" class="cta"><?php the_title(); ?></a>
	    <?php endforeach; wp_reset_postdata(); ?>
	<?php endif; 

// if( get_field('cta_link_url') ) {
//     echo get_field('cta_link_url');
// }

} // End CTA

// Edit post link
if(!$hide_edit_post_link)
	edit_post_link();
} // End edit post link

wp_reset_query();
} // End Content

}