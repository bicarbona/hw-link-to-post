<?php
/* This class must be included in another file and included later so we don't get an error about HeadwayBlockOptionsAPI class not existing. */

  class link_to_postBlockOptions extends HeadwayBlockOptionsAPI {

    public $tabs = array(
      'my-settings' => 'Settings', 
    );

  function modify_arguments($args) {

  	$block = $args['block'];

//	$this->tab_notices['my-settings'] = self::picus(). HeadwayBlocksData::get_block_name($args['block_id']) . HeadwayBlockAPI::get_setting($block, 'post-type');

	$post_type = HeadwayBlockAPI::get_setting($block, 'post-type');


/**

Options

**/
			$this->inputs['my-settings']['post-type'] = array(
				'type' => 'select',
				'name' => 'post-type',
				'label' => 'Post Type',
				//'tooltip' => HeadwayBlockAPI::get_setting($block, 'post-type'),
				'options' => 'get_post_types()',
				'callback' => '
					reloadBlockOptions()'
			);

			$this->inputs['my-settings']['categories'] = array(
				'type' => 'multi-select',
				'name' => 'categories',
				'label' => 'Categories',
				'options' => 'get_categories()',
				//'tooltip' => var_dump(HeadwayBlockAPI::get_setting($block, 'categories')),

				'tooltip' =>  HeadwayBlockAPI::get_setting($block, 'categories')[0]

			);

			$this->inputs['my-settings']['post-id'] = array(
				'type' => 'multi-select',
				'name' => 'post-id',
				'label' => 'Post',
				'options' => self::hw_get_post($block)
			);

			$this->inputs['my-settings']['hide-edit-post-link'] = array(
				'type' => 'checkbox',
				'name' => 'hide-edit-post-link', 
				'label' => 'Hide Edit Post Link',
				'default' => false,
			);

			// Heading
			$this->inputs['my-settings']['heading-title'] = array(
				'name'	 => 'heading-title',
				'type'	 => 'heading',
				'label'	 => 'Title'
			);

			$this->inputs['my-settings']['title-html-tag'] = array(
					'type' => 'select',
					'name' => 'title-html-tag',
					'label' => 'Title HTML tag',
					'default' => 'h2',
					'options' => array(
						'h1' => '&lt;H1&gt;',
						'h2' => '&lt;H2&gt;',
						'h3' => '&lt;H3&gt;',
						'h4' => '&lt;H4&gt;',
						'h5' => '&lt;H5&gt;',
						'h6' => '&lt;H6&gt;',
						'span' => '&lt;span&gt;'
					)
			);
			
			$this->inputs['my-settings']['title-link'] = array(
				'type' => 'checkbox',
				'name' => 'title-link',
				'label' => 'Link Title?'
			);

			$this->inputs['my-settings']['title-shorten'] = array(
				'type' => 'checkbox',
				'name' => 'title-shorten',
				'label' => 'Truncate Title?',
				'toggle'  => array(
					'true' => array(
						'show' => array(
							'#input-title-limit'
						),
					),
					'false' => array(
						'hide' => array(
							'#input-title-limit'
						),
					)
				),
			);

			$this->inputs['my-settings']['hide-title'] = array(
				'type' => 'checkbox',
				'name' => 'hide-title', 
				'label' => 'Hide title',
				'default' => false,
			);

			$this->inputs['my-settings']['title-limit'] = array(
				'type' => 'text',
				'name' => 'title-limit', 
				'label' => 'Limit Characters',
				'default' => '20',
			);
	  }

	
	function get_post_types() {
		
		$post_type_options = array();

		$post_types = get_post_types(false, 'objects'); 
		foreach($post_types as $post_type_id => $post_type){
			
			//Make sure the post type is not an excluded post type. 
			if(in_array($post_type_id, array('revision', 'nav_menu_item'))) 
				continue;
			$post_type_options[$post_type_id] = $post_type->labels->name;
		
		}
		
		return $post_type_options;
		
	}


	function get_categories($block) {
		
		$category_options = array();
		
		// $args = array(
		// 	'type' => 'skider',
		// 	'orderby' => $this->get_post_types(),
		// 	'order' => 'ASC'
		// );

		$categories_select_query = get_categories();
		
		foreach ($categories_select_query as $category)
			$category_options[$category->term_id] = $category->name;

		return $category_options;
	
	}

	function hw_get_post($block) {

		$category_options = array();

		$args = array(
		    //'numberposts'	=> -1,
		    'orderby'		=> 'post_date',
		    'order'			=> 'DESC',
		    'category'		=>	HeadwayBlockAPI::get_setting($block, 'categories')[0],
		    'post_type'     =>	HeadwayBlockAPI::get_setting($block, 'post-type'), // Dolezite takto sa predava parameter v ramci options
		    'suppress_filters' => true
		);

		//$args['category'] = HeadwayBlockAPI::get_setting($block, 'categories');

		/**

		 */
		$posts = get_posts($args);
		 
		foreach ( $posts as $post ) {
			$category_options[ $post->ID ] = get_the_title( $post->ID );
		}

		return ($category_options);
	}

 }